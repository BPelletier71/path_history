#include "path_history_service/Node.hpp"

namespace path_history
{

    Node::Node(const std::string name)
        : rclcpp::Node(name)
    {

        history_size_ = this->declare_parameter("size",10);
        frequency_ = this->declare_parameter("frequency",10);

        odometry_sub_ = this->create_subscription<nav_msgs::msg::Odometry>(
            "~/odometry", 10,
            [this](nav_msgs::msg::Odometry::UniquePtr msg)
            {
                if (odom_step_%frequency_ == 0)
                {
                    if (odometry_history_.size() < history_size_)
                    {
                        odometry_history_.push_back(*msg);
                    }
                    else
                    {
                        odometry_history_.erase(odometry_history_.begin());
                        odometry_history_.push_back(*msg);
                    }
                }
                odom_step_++;
            });

        geopose_sub_ = this->create_subscription<geographic_msgs::msg::GeoPoint>(
            "~/geopose", 10,
            [this](geographic_msgs::msg::GeoPoint::UniquePtr msg)
            {
                if (geo_step_%frequency_ == 0)
                {
                    if (geopose_history_.size() < history_size_)
                    {
                        geopose_history_.push_back(*msg);
                    }
                    else
                    {
                        geopose_history_.erase(geopose_history_.begin());
                        geopose_history_.push_back(*msg);
                    }
                }
                geo_step_++;
            });

        odometry_history_request_sub_ = this->create_subscription<std_msgs::msg::Empty>("~/odometry/request", 10,
            [this](std_msgs::msg::Empty::UniquePtr request)
                {
                    this->send_odometry_history_();
                }
        );
        odometry_history_response_pub_ = this->create_publisher<path_history_interfaces::msg::OdomHistory>("~/odometry/response",10);
        
        geopose_history_request_sub_ = this->create_subscription<std_msgs::msg::Empty>("~/geopose/request", 10,
            [this](std_msgs::msg::Empty::UniquePtr request)
                {
                    this->send_geopose_history_();
                }
        );
        geopose_history_response_pub_ = this->create_publisher<path_history_interfaces::msg::GeoposeHistory>("~/geopose/response",10);

    }

    void Node::send_odometry_history_()
    {
        path_history_interfaces::msg::OdomHistory msg;
        msg.odometry_history = odometry_history_;
        odometry_history_response_pub_->publish(msg);
    }

    void Node::send_geopose_history_()
    {
        path_history_interfaces::msg::GeoposeHistory msg;
        msg.geopose_history = geopose_history_;
        geopose_history_response_pub_->publish(msg);
    }
}