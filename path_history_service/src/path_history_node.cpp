#include "path_history_service/Node.hpp"
#include "rclcpp/rclcpp.hpp"

int main(int argc, char *argv[])
{
    rclcpp::init(argc, argv);
    auto node = std::make_shared<path_history::Node>("path_history");
    rclcpp::spin(node);
    rclcpp::shutdown();
    return 0;
}
