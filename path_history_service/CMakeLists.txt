cmake_minimum_required(VERSION 3.8)
project(path_history_service)

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(std_msgs REQUIRED)
find_package(std_srvs REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(geographic_msgs REQUIRED)
find_package(path_history_interfaces REQUIRED)

###########
## Build ##
###########

add_library(path_history_service SHARED
  src/Node.cpp
)

target_include_directories(path_history_service PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>)

ament_target_dependencies(path_history_service
  rclcpp
  std_msgs
  std_srvs
  nav_msgs
  geographic_msgs
  path_history_interfaces
  )

install(DIRECTORY include/
  DESTINATION include
)

install(
  TARGETS path_history_service
  EXPORT export_path_history_service
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
)

ament_export_include_directories(include)
ament_export_targets(export_path_history_service HAS_LIBRARY_TARGET)

add_executable(path_history_node src/path_history_node.cpp)
target_link_libraries(path_history_node path_history_service)
ament_target_dependencies(path_history_node rclcpp std_msgs std_srvs nav_msgs geographic_msgs path_history_interfaces)

 install(TARGETS
 path_history_node
   DESTINATION lib/${PROJECT_NAME})

ament_package()