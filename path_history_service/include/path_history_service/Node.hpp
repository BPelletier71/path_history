#ifndef __PATH_HISTORY_NODE_HPP__
#define __PATH_HISTORY_NODE_HPP__

#include <string>

#include "rclcpp/rclcpp.hpp"
#include "rclcpp/time.hpp"
#include "std_msgs/msg/empty.hpp"
#include "nav_msgs/msg/odometry.hpp"
#include "geographic_msgs/msg/geo_point.hpp"

#include "path_history_interfaces/msg/geopose_history.hpp"
#include "path_history_interfaces/msg/odom_history.hpp"

using namespace std::chrono_literals;

namespace path_history
{
    class Node : public rclcpp::Node
    {
    public:
        Node(const std::string name);
        ~Node(){};

    private:
        void send_odometry_history_();
        void send_geopose_history_();

        int frequency_;
        int odom_step_ = 0;
        int geo_step_ = 0;
        int history_size_;
        std::vector<nav_msgs::msg::Odometry> odometry_history_;
        std::vector<geographic_msgs::msg::GeoPoint> geopose_history_;

        rclcpp::Subscription<nav_msgs::msg::Odometry>::SharedPtr odometry_sub_;
        rclcpp::Subscription<geographic_msgs::msg::GeoPoint>::SharedPtr geopose_sub_;
        rclcpp::Subscription<std_msgs::msg::Empty>::SharedPtr odometry_history_request_sub_;
        rclcpp::Subscription<std_msgs::msg::Empty>::SharedPtr geopose_history_request_sub_;
        rclcpp::Publisher<path_history_interfaces::msg::OdomHistory>::SharedPtr odometry_history_response_pub_;
        rclcpp::Publisher<path_history_interfaces::msg::GeoposeHistory>::SharedPtr geopose_history_response_pub_;
    };

}

#endif