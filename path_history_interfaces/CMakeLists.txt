cmake_minimum_required(VERSION 3.8)
project(path_history_interfaces)

if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 17)
endif()

find_package(ament_cmake REQUIRED)
find_package(rclcpp REQUIRED)
find_package(rosidl_default_generators REQUIRED)
find_package(builtin_interfaces REQUIRED)
find_package(std_msgs REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(geographic_msgs REQUIRED)

rosidl_generate_interfaces(path_history_interfaces
  "msg/GeoposeHistory.msg"
  "msg/OdomHistory.msg"
  DEPENDENCIES builtin_interfaces std_msgs nav_msgs geographic_msgs
)
rosidl_get_typesupport_target(cpp_typesupport_target path_history_interfaces "rosidl_typesupport_cpp")

ament_package()