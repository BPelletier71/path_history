# Path History Service

Package for the creation of a history node that stocks odometry and geopoint messages and publishes them on request.

## Odometry and GeoPoint history

The package requires:
    - std_msgs
    - nav_msgs
    - geographic_msgs

### Parameters:
    - size: history size
    - frequency: frequency at which messages are stored (compared to initial frequency of subscriber topics)

### Subscriber topics:

~/odometry: topic sending odometry messages (of type nav_msgs::msg::Odometry)
~/geopose: topic sending geopoint messages (geographic_msgs::msg::GeoPoint)


### Package topics:

~/(odometry or geopose)/request: waits for a std_msgs::msg::Empty message before sending the corresponding history on the response topic.
~/(odometry or geopose)/response: sends an array of odometry/geopose messages.
